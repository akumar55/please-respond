const WebSocket = require('ws'); //Import ws library

//Data objects for storing relevant parsed data
countryCount = {}; //Object for country counts
total = 0; //total RSVPs
future_date = 0; //Date of 
future_url = '';

var interval = 60000; //Default time interval
//Parse input arguments for defined time interval
if(process.argv.slice(2)[0]){
	interval = parseInt(process.argv.slice(2)[0])*1000;
}
 
//Initialize websocket connection to meetup.com
const ws = new WebSocket('ws://stream.meetup.com/2/rsvps');

//On connection open, start timer
ws.on('open', function open() {
	console.log('Starting data collection');
  setTimeout(onInterval, interval); //Timer even based on defined interval
});

//Function to run once interval has elapsed
function onInterval(){ 
	console.log("Closing Connection");
	ws.close(); //Close connection
	
	output = format_output(); //Format collected data

	console.log(output); //Ouput data
}

//Code to run each time data is received through websocket connection
ws.on('message', function incoming(data) {
	rsvp = JSON.parse(data); //Parse string to JSON
	total += 1; //Increment total count

	//If date of current event is later than stored event, replace data
	if(rsvp.event.time>future_date){
		future_date = rsvp.event.time;
		future_url = rsvp.event.event_url;
	}

	//Add of increment country counts in countryCount object
	country = rsvp.group.group_country;
  if(!countryCount[country]){
  	countryCount[country] = 1;
  }else{
  	countryCount[country] += 1;
  }
  //console.log(typeof(data));
});

function format_output(){
	//Sort coutries by count:
	//- Create new array object using countryCount object keys
	//- Sort keys by accessing object values and using sort function
	countriesSorted = Object.keys(countryCount).sort(
		function(a,b){return countryCount[a]-countryCount[b]}
	).reverse();

	//Create new date string using epoch time:
	//- Create new Date object 
	//- Set UTC time using Epoch milliseconds
	//- Access Date object properties and create date string using ES2015 template literals
	var dateUTC = new Date(0);
	dateUTC.setUTCMilliseconds(future_date);
	future_date_string = 
		`${dateUTC.getFullYear()}-${("0"+dateUTC.getMonth()).slice(-2)}-${("0"+dateUTC.getDate()).slice(-2)} `+
		`${("0"+dateUTC.getHours()).slice(-2)}:${("0"+dateUTC.getMinutes()).slice(-2)}`
	
	//Format string for ouput using Date, URL, and country counts by accessing Object data using sorted keys
	formatted_string = 
		total
		+ "," + future_date_string
		+ "," + future_url
		+ "," + countriesSorted[0]
		+ "," + countryCount[countriesSorted[0]]
		+ "," + countriesSorted[1]
		+ "," + countryCount[countriesSorted[1]]
		+ "," + countriesSorted[2]
		+ "," + countryCount[countriesSorted[2]];

	return formatted_string;
}